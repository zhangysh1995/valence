# XPS
A collection of calling context encoding passes.

# Build
Put the directory under `llvm-src/lib/Transforms` and add directory XPS to
`llvm-src/lib/Transforms/CMakeLists.txt`. Then it should build together
with LLVM when you build LLVM.

# PCCE
## Instrument
```
$ opt -load $LLVM_LIB_DIR/XPS.so -pcce xxx.ll
```
The acyclic context is stored in integer `pcce_id`. Cyclic contexts are
handled by two function calls `pcce_push_ctx` and `pcce_pop_ctx`. You
should implement these interfaces.

# Valence
The acyclic and cyclic encodings are in two separate passes.
## Instrument Acyclic CG
```
$ opt -load $LLVM_LIB_DIR/XPS.so -scce xxx.ll
```
The static bit vector is implemented as a series of integers such as `scce_ctx0`,
`scce_ctx1` etc. 

## Instrument Cyclic CG
```
$ opt -load $LLVM_LIB_DIRXPS.so -scce-cyclic -cyclic-mode=1 xxx.ll
```
When `cyclic-mode` is 1, a function call is insert around each cycle call site.
The interfaces are `scce_push_edge` and `scce_pop_edge`.

When `cyclic-mode` is 2, an inlined version of push/pop is implemented with a buffer.
The buffer is `scce_cycle_ctx`, and buffer size is `scce_cycle_ctx_size`. Each push
will try to store it in the buffer first, and only make function calls when the buffer
is full.


# HCCE
Cyclic CG encoding is the same as Valence, but use PCCE for acyclic encoding now.
## Instrument Acyclic CG
```
$ opt -load $LLVM_LIB_DIR/XPS.so -pcce-ac xxx.ll
```
The acyclic context is stored in integer `pcce_id`.

