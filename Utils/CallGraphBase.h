#ifndef LLVM_CALLGRAPHBASE_H
#define LLVM_CALLGRAPHBASE_H

#include <string>
#include <fstream>
#include <cassert>
#include <Utils/PassCommons.h>

typedef std::string string;

using namespace llvm;

namespace xps {
// A base class that provides a complete call graph with
// indirect calls
class CallGraphBase {
protected:
  int _ic_id;
  std::map<int, Instruction*> _ics;
  std::map<Function*, std::set<Instruction*>> _ic_callers;
  std::map<Instruction*, std::set<Function *>> _ic_targets;
public:
  CallGraphBase() {

  }

  void constructICIDMap(Module* m) {
    int ic_id = 0;
    for (auto& F: *m) {
      for (auto& B: F) {
        for (auto& I: B) {
          if (CallSite CS = CallSite(&I)) {
            if (CS.isIndirectCall()) {
              _ics[ic_id] = CS.getInstruction();
              ic_id++;
            }
          }
        }
      }
    }
  }

  void loadICProfile(string& file, Module* m) {
    constructICIDMap(m);

    std::ifstream ifs(file);
    if (!ifs.good()) {
      errs() << "open file " << file << " failed\n";
      return;
    }
    
    string line;
    while (std::getline(ifs, line)) {
      int sp = line.find(' ');
      int f1 = std::stoi(line.substr(0, sp));
      string f2 = line.substr(sp + 1);
      Instruction* IC = _ics.at(f1);
      if (_ic_targets.find(IC) == _ic_targets.end()) {
        _ic_targets[IC] = std::set<Function *>();
      }
      auto F = m->getFunction(f2);
      assert(F);
      _ic_targets[IC].insert(F);

      if (_ic_callers.find(F) == _ic_callers.end()) {
        _ic_callers[F] = std::set<Instruction*>();
      }
      _ic_callers[F].insert(IC);
    }
  }

  void getCallees(CallSite CS, std::set<Function *> &callees) {
    callees.insert(get_callee(CS));

    auto I = CS.getInstruction();
    if (_ic_targets.find(I) != _ic_targets.end()) {
      auto& set = _ic_targets[I];
      callees.insert(set.begin(), set.end());
    }
  }
  
  void getCallers(Function* F, std::set<Instruction*>& callers) {
    for (auto U: F->users()) {
      if (CallSite CS = CallSite(U)) {
        auto I = CS.getInstruction();
        callers.insert(I);
      }
    }

    if (_ic_callers.find(F) != _ic_callers.end()) {
      auto set = _ic_callers[F];
      callers.insert(set.begin(), set.end());
    }
  }

  void getICCallers(Function* F, std::set<Instruction*>& callers) {
    if (_ic_callers.find(F) != _ic_callers.end()) {
      auto set = _ic_callers[F];
      callers.insert(set.begin(), set.end());
    }
  }

  Function* getHighestCallee(std::map<Function*, int>& levels,
                            CallSite CS) {
    if (!CS.isIndirectCall()) {
      return get_callee(CS);
    }
    else {
      auto& set = _ic_targets[CS.getInstruction()];
      int highest = 0;
      Function* final = NULL;
      for (auto& callee: set) {
        if (levels.at(callee) > highest) {
          highest = levels.at(callee);
          final = callee;
        }
      }
      return final;
    }
  }
};
}

#endif