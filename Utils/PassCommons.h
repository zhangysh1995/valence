//
// Created by tzhou on 12/29/17.
//

#ifndef LLVM_XPS_PASSCOMMONS_H
#define LLVM_XPS_PASSCOMMONS_H

#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/DebugInfo.h"
#include <llvm/IR/CallSite.h>
#include <llvm/IR/Value.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Constant.h>
#include <llvm/IR/Constants.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/Transforms/Utils/BasicBlockUtils.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/IR/LegacyPassManager.h>
#include <set>

namespace xps {

using namespace llvm;

llvm::Function* get_callee(llvm::CallSite CS);
void get_callers(llvm::Function* F, std::set<llvm::Instruction*>& insts);
std::set<Function*> get_callees(Function* F);
std::set<Function*> get_defined_callees(Function* F);
std::set<llvm::Instruction*> get_callers(llvm::Function* F);
std::set<llvm::Instruction*> get_succ_insts(llvm::Instruction* I);
void print_call(llvm::CallSite CS);
llvm::Function* get_main_function(llvm::Module* M);
bool has_definition(llvm::Function* F);
void dumpI(llvm::Instruction* I);
void insert_new_malloc(Module* m, std::string prefix="ben_", bool add_id=false);
void insert_new_calloc(Module* m, std::string prefix="ben_", bool add_id=false);
void insert_new_realloc(Module* m, std::string prefix="ben_", bool add_id=false);
void insert_new__Znam(Module* m, std::string prefix="ben_", bool add_id=false);
void insert_new__Znwm(Module* m, std::string prefix="ben_", bool add_id=false);
void insert_new_allocs(Module* m, std::string prefix="ben_", bool add_id=false);

}

#endif //LLVM_XPS_PASSCOMMONS_H
