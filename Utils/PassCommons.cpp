//
// Created by tzhou on 12/29/17.
//

#include "PassCommons.h"


using namespace llvm;

namespace xps {

void printCall(CallSite CS) {
  auto I = CS.getInstruction();
  errs() << I->getFunction()->getName() << ":";
  I->dump();
}

void dumpI(Instruction* I) {
  outs() << I->getFunction()->getName();
  I->print(outs());
  outs() << "\n";
}

/*
 * Maybe use Value.stripPointerCasts
 *
 */
Function* get_callee(CallSite CS) {
  auto I = CS.getInstruction();
  if (CS.isIndirectCall()) {
    return NULL;
  }

  Value* v = CS.getCalledValue();
  if (!v) {
    return NULL;
  }
  Function* f;

  if (auto CE = dyn_cast<ConstantExpr>(v)) {
    v = CE->getOperand(0);
  }

  f = dyn_cast<Function>(v);
  if (!f) {
    GlobalAlias* ga = dyn_cast<GlobalAlias>(v);
    if (ga) {
      f = dyn_cast<Function>(ga->getAliasee());
    }
    else {
      errs() << "Unresolved callee: \n";
      I->dump();

    }
  }

  //assert(f);
  return f;
}

GlobalAlias* get_aliaser(Function* F) {
  auto& list = F->getParent()->getAliasList();
  for (auto& ga: list) {
    if (dyn_cast<Function>(ga.getAliasee()) == F) {
      return &ga;
    }
  }
  return NULL;
}

void get_callers(Function* F, std::set<Instruction*>& insts) {
  GlobalAlias* aliaser = get_aliaser(F);
  //outs() << ->getName() << "\n";
  for (auto User: F->users()) {
    //User->dump();

    if (auto CE = dyn_cast<ConstantExpr>(User)) {
      for (auto SU: CE->users()) {
        if (CallSite CS = CallSite(SU)) {
          if (get_callee(CS) == F) {
            insts.insert(CS.getInstruction());
          }
        }
      }
    }
    else {
      if (CallSite CS = CallSite(User)) {
        if (get_callee(CS) == F) {
          insts.insert(CS.getInstruction());
        }
      }

    }
  }

  if (aliaser) {
    for (auto User: aliaser->users()) {
      //User->dump();

      if (auto CE = dyn_cast<ConstantExpr>(User)) {
        for (auto SU: CE->users()) {
          if (CallSite CS = CallSite(SU)) {
            if (get_callee(CS) == F) {
              insts.insert(CS.getInstruction());
            }
          }
        }
      }
      else {
        if (CallSite CS = CallSite(User)) {
          if (get_callee(CS) == F) {
            insts.insert(CS.getInstruction());
          }
        }

      }
    }
  }
}

std::set<Instruction*> get_callers(Function* F) {
  std::set<Instruction*> callers;
  get_callers(F, callers);
  return callers;
}

/**
 * Not include indirect calls
 * @param F
 * @return
 */
std::set<Function*> get_callees(Function* F) {
  std::set<Function*> callees;
  for (auto& B: *F) {
    for (auto& I: B) {
      if (CallSite CS = CallSite(&I)) {
        if (Function* f = get_callee(CS)) {
          callees.insert(f);
        }
      }
    }
  }
  return callees;
}

/**
 * Not include indirect calls, intrinsic calls and library calls
 * @param F
 * @return
 */
std::set<Function*> get_defined_callees(Function* F) {
  std::set<Function*> callees;
  for (auto& B: *F) {
    for (auto& I: B) {
      if (CallSite CS = CallSite(&I)) {
        if (Function* f = get_callee(CS)) {
          if (has_definition(f)) {
            callees.insert(f);
          }
        }
      }
    }
  }
  return callees;
}

Function* get_main_function(Module* M) {
  Function* f = M->getFunction("main");
  if (!f) {
    f = M->getFunction("MAIN_");
  }
  return f;
}

bool has_definition(Function* F) {
  if (!F) {
    return false;
  }

  // This fails astar's _ZN15largesolidarrayIP6regobjE6createEi
//  if (F->hasExactDefinition()) {
//    return true;
//  }
  if (F->isIntrinsic()) {
    return false;
  }

  if (!F->isDeclaration()) {
    return true;
  }

  std::string name = F->getName().str();
  std::vector<std::string> funcs = {
      "malloc",
      "calloc",
      "realloc",
      "free",
      "_Znam",
      "_Znwm",
      "_ZdaPv",
      "_ZdlPv",
  };

  for (auto f: funcs) {
    if (name == "ctx_" + f) {
      return true;
    }
    else if (name == f) {
      return true;
    }
  }

  return false;
}

void replacePhiUseInNormalDest(InvokeInst* I, BasicBlock* New) {
  //I->getParent()->replaceSuccessorsPhiUsesWith(Block);
  BasicBlock* Succ = I->getNormalDest();
  
    // N.B. Succ might not be a complete BasicBlock, so don't assume
    // that it ends with a non-phi instruction.
  for (auto& II: *Succ) {
      PHINode *PN = dyn_cast<PHINode>(&II);
      if (!PN)
        break;
      int i;
      while ((i = PN->getBasicBlockIndex(I->getParent())) >= 0)
        PN->setIncomingBlock(i, New);
  }

}

void replacePhiUseInUnwindDest(InvokeInst* I, BasicBlock* New) {
  //I->getParent()->replaceSuccessorsPhiUsesWith(Block);
  BasicBlock* Succ = I->getUnwindDest();
  
    // N.B. Succ might not be a complete BasicBlock, so don't assume
    // that it ends with a non-phi instruction.
  for (auto& II: *Succ) {
      PHINode *PN = dyn_cast<PHINode>(&II);
      if (!PN)
        break;
      int i;
      while ((i = PN->getBasicBlockIndex(I->getParent())) >= 0)
        PN->setIncomingBlock(i, New);
  }  

}

TerminatorInst* insertNewNormalDest(InvokeInst* I) {
  BasicBlock* old_suc = I->getNormalDest();
  BasicBlock* new_suc = BasicBlock::Create(
           I->getContext(), "", I->getParent()->getParent(), old_suc);
  new_suc->setName(I->getParent()->getName() + ".newNormalDest");

  //
  // outs() << "this block: " << I->getParent()->getName() << "\n";
  // outs() << "dest name: " << normalDest->getName() << "\n";
  
  
  if (dyn_cast<PHINode>(old_suc->begin())) {
    //I->getParent()->replaceSuccessorsPhiUsesWith(Block);
    replacePhiUseInNormalDest(I, new_suc);
  }
  
  TerminatorInst* Term = BranchInst::Create(old_suc, new_suc);
  I->setNormalDest(new_suc);
  return Term;
}

TerminatorInst* insertNewUnwindDest(InvokeInst* I) {
  BasicBlock* old_suc = I->getUnwindDest();
  BasicBlock* new_suc = BasicBlock::Create(
           I->getContext(), "", I->getParent()->getParent(), old_suc);
  new_suc->setName(I->getParent()->getName() + ".newUnwindDest");

  LandingPadInst* lpad = I->getLandingPadInst();
  
  //
  // outs() << "this block: " << I->getParent()->getName() << "\n";
  // outs() << "dest name: " << normalDest->getName() << "\n";
  
  
  if (dyn_cast<PHINode>(old_suc->begin())) {
    //I->getParent()->replaceSuccessorsPhiUsesWith(Block);
    //replacePhiUse(I, new_suc);
  }
  
  TerminatorInst* Term = BranchInst::Create(old_suc, new_suc);
  I->setUnwindDest(new_suc);

  //lpad->removeFromParent();
  //lpad->insertBefore(Term);

  return Term;
}

std::set<Instruction*> get_succ_insts(Instruction* I) {
  std::set<Instruction*> nodes;
  if (auto i = dyn_cast<InvokeInst>(I)) {
    //nodes.insert(i->getNormalDest()->getFirstNonPHI());
    nodes.insert(insertNewNormalDest(i));
    //assert(i->getUnwindDest()->getSinglePredecessor());
    //i->getLandingPadInst();
    LandingPadInst* lpad = i->getLandingPadInst();
    //nodes.insert(insertNewUnwindDest(i));
    //nodes.insert(i->getLandingPadInst()->getNextNode());
  }
  else {
    assert(I->getNextNode());
    nodes.insert(I->getNextNode());
  }
  return nodes;
}

}
