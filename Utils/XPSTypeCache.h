//
// Created by tzhou on 12/24/17.
//

#ifndef LLVM_XPSTYPECACHE_H
#define LLVM_XPSTYPECACHE_H

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/DerivedTypes.h"

/// This file does not use namespace llvm because it's a header file

namespace xps {

#define zpp(arg) \
  std::printf(#arg ": %p", arg); \
  std::printf("\n");

#define zpl(fmt, args...) \
  std::printf(fmt, ##args); \
  std::printf("\n");

#define zps(arg) \
  std::printf(#arg ": %s", arg.c_str()); \
  std::printf("\n");

#define zpd(arg) \
  std::printf(#arg ": %d", arg); \
  std::printf("\n");

#define zpw() \
  std::printf("word: %s", _word.c_str()); \
  std::printf("\n");


/// This structure provides a set of types that are commonly used
/// during IR emission.
class XPSTypeCache {
public:
  void initialize(llvm::LLVMContext& ctx);

  /// void
  llvm::Type *VoidTy;

  /// i8, i16, i32, and i64
  llvm::IntegerType *Int8Ty, *Int16Ty, *Int32Ty, *Int64Ty;
  /// float, double
  llvm::Type *FloatTy, *DoubleTy;

  llvm::PointerType *Int32PtrTy;
  llvm::PointerType *Int64PtrTy;

  /// int
  llvm::IntegerType *IntTy;

  /// intptr_t, size_t, and ptrdiff_t, which we assume are the same size.
  union {
    llvm::IntegerType *IntPtrTy;
    llvm::IntegerType *SizeTy;
    llvm::IntegerType *PtrDiffTy;
  };

  /// void* in address space 0
  union {
    llvm::PointerType *VoidPtrTy;
    llvm::PointerType *Int8PtrTy;
  };

  /// void** in address space 0
  union {
    llvm::PointerType *VoidPtrPtrTy;
    llvm::PointerType *Int8PtrPtrTy;
  };

  /// The size and alignment of the builtin C type 'int'.  This comes
  /// up enough in various ABI lowering tasks to be worth pre-computing.
  union {
    unsigned char IntSizeInBytes;
    unsigned char IntAlignInBytes;
  };

  /// The width of a pointer into the generic address space.
  unsigned char PointerWidthInBits;

  /// The size and alignment of a pointer into the generic address space.
  union {
    unsigned char PointerAlignInBytes;
    unsigned char PointerSizeInBytes;
  };

  /// The size and alignment of size_t.
  union {
    unsigned char SizeSizeInBytes; // sizeof(size_t)
    unsigned char SizeAlignInBytes;
  };

};
}
#endif //LLVM_XPSTYPECACHE_H
